from rest_framework import serializers

from django_monstack_icinga2.django_monstack_icinga2.models_icinga2_director import (
    IcingaHost,
)


class IcingaHostSerializer(serializers.ModelSerializer):
    class Meta:
        model = IcingaHost
        fields = ("id", "object_name")
