from urllib.parse import urljoin

import requests
from rest_framework import permissions
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from django_cdstack_deploy.django_cdstack_deploy.models import *
from django_cdstack_models.django_cdstack_models.func import get_or_create_cmdb_host


@api_view(["POST"])
@permission_classes((permissions.AllowAny,))
def hosts(request):
    try:
        tmp_int = int(request.data.get("deployment_id"))
    except ValueError:
        content = {"error": "Instance-ID not found or incorrect!"}
        return Response(content, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

    if request.method == "POST":
        # Check if Deployment is allowed
        try:
            instance = CmdbInstance.objects.get(id=request.data.get("deployment_id"))
        except CmdbInstance.DoesNotExist:
            content = {"error": "Instance-ID not found or incorrect!"}
            return Response(content, status=status.HTTP_404_NOT_FOUND)

        try:
            api_key = instance.api_key
        except CmdbApiKeyInstance.DoesNotExist:
            content = {"error": "Instance-ApiKey not found or incorrect!"}
            return Response(content, status=status.HTTP_404_NOT_FOUND)

        if not api_key.key == request.data.get("deployment_pw"):
            content = {"error": "Instance-ApiKey not found or incorrect!"}
            return Response(content, status=status.HTTP_404_NOT_FOUND)

        if len(request.data.get("fqdn")) <= 3:
            content = {"error": "Hostname invalid / too short!"}
            return Response(content, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        host = get_or_create_cmdb_host(request.data.get("fqdn"), instance)

        # Update OS
        try:
            os_family = CmdbOsFamily.objects.get(
                name__iexact=request.data.get("os_family")
            )
        except CmdbOsFamily.DoesNotExist:
            content = {"error": "os_family not found or incorrect!"}
            return Response(content, status=status.HTTP_404_NOT_FOUND)

        try:
            os_version = CmdbOsVersion.objects.get(
                family=os_family, name__iexact=request.data.get("os_version")
            )
        except CmdbOsVersion.DoesNotExist:
            os_version = CmdbOsVersion(
                family=os_family, name=request.data.get("os_version")
            )
            os_version.save(force_insert=True)

        if not host.os == os_version:
            host.os = os_version
            host.save()

        session = requests.Session()
        session.headers = {
            "Accept": "application/json",
        }

        session.auth = (settings.ICINGA_MAIN_API_USER, settings.ICINGA_MAIN_API_PASSWD)

        request_url = urljoin(
            settings.ICINGA_MAIN_API_URL, "v1/actions/generate-ticket"
        )

        payload = dict()
        payload["cn"] = host.monitoring_id

        # create arguments for the request
        request_args = {"url": request_url, "json": payload, "verify": False}

        # do the request
        response = session.post(**request_args)
        monitoring_data = response.json()

        content = {
            "cn": host.monitoring_id,
            "version": CmdbVarsGlobalDefault.objects.get(
                name="monitoring_icinga2_client_version"
            ).value,
            "ticket": monitoring_data["results"][0]["ticket"],
            "message": monitoring_data["results"][0]["status"],
        }
    else:
        content = "SOMETHING BROKE BADLY"
        return Response(content, status=status.HTTP_501_NOT_IMPLEMENTED)

    return Response(content, status=status.HTTP_200_OK)
