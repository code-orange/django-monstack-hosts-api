from django.urls import path

from . import views

urlpatterns = [
    path("hosts", views.hosts),
]
